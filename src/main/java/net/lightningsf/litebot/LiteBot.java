package net.lightningsf.litebot;

import org.jibble.pircbot.PircBot;

/**
 * Created by xiurobert on 5/7/2015.
 * @author xiurobert
 */

/*
Note to self:
String[] something = message.split(//chars e.g. " ");, breaks up
message into {"part1", "part2"} without spaces

 */

public class LiteBot extends PircBot {
    public String creator = "xiurobert";

    public String prefix = "^";

    public LiteBot() {
        this.setName("LiteBot");
    }
    protected void onMessage(
            String channel,
            String sender,
            String login,
            String hostname,
            String message) {
        if (message.equalsIgnoreCase(prefix + "help")) {
            sendNotice(sender, "You have performed command" + message);
            sendNotice(sender, "Here is a list of things I can do");
            sendNotice(sender, "kick, ban, mute, quit, stop, reconnect, version");
            sendNotice(sender, "For detailed help, type ^help [command]");
        } else if (message.equalsIgnoreCase("help")) {
            sendNotice(sender, "LiteBot: Would you like LiteBot help?");
            sendNotice(sender, "LiteBot: LiteBot commands are prefixed with a" + " " + prefix);
            sendNotice(sender, "LiteBot: E.g. ^help");
        }

        if (message.startsWith("^ban")) {
            if (sender == creator) {
                String usertoban = message.split(" ")[1];
                ban(channel, usertoban);
                sendMessage(channel, "Banned" + usertoban);
            } else {
                sendMessage(channel, sender + "tried to initiate an attack on the channel");
                sendMessage(channel, "Kicked" + sender + " for trying to attack the channel");
                kick(sender, "Do not harrass LiteBot");
            }
        }

        if (message.startsWith("^kick")) {
            if (sender == creator) {
                String usertokick = message.split(" ")[1];
                kick(channel, usertokick);
                sendMessage(channel, "Kicked" + usertokick);
            } else {
                sendMessage(channel, sender + "tried to initiate an attack on the channel");
                sendMessage(channel, "Kicked" + sender + " for trying to attack the channel");
                kick(sender, "Do not harrass LiteBot");
            }
        }


    }
}
